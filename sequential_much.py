# python 3.8
import asyncio


async def level3(i):
    print(f"task {i} reached the bottom, exiting...")


async def level2(i):
    print(f"task {i} entering level 3")
    result = await level3(i)
    print(f"task {i} exiting level 3")


async def level1(i):
    print(f"task {i} entering level 2")
    result = await level2(i)
    print(f"task {i} exiting level 2")


async def main():
    return await asyncio.gather(*[level1(i) for i in range(3)])


asyncio.run(main())