📣 Forked to [`pyo3-futures`](https://gitlab.com/ThibaultLemaire/pyo3-futures) and [`sterling`](https://gitlab.com/ThibaultLemaire/sterling)

---

# PyO3 Futures Sandbox

My dirty, throw-away, prototype code for asyncio-driven rust futures.

So I've finally decided to push this to gitlab because I've got something fairly decent now.

The idea was that the asynchronous models of Python and Rust aren't that different to not be interoperable.

Without going into too much detail, here's the idea: Python has it's own event loop: asyncio (In most cases at least, or a compatible one like uvloop). What if that event loop was able to drive Rust futures as `Awaitable` objects?

## See it in action

```sh
cargo build --release
python test_rust_lib.py
```

You can then fiddle with `test_rust_lib.py` to try different coroutines exposed by the lib.
Then take a look at `src/lib.rs` for the sorcery. All the interesting stuff is at the top.

## Why I care

You could for example leverage async Rust crates from Python. Let's take my specific example: I have some Python code connecting to MongoDB, and I'd like that to be async (as DB accesses are by definition I/O bound). Currently I use `motor`, which does the job. But `motor` is actually an ugly monkeypatch of `pymongo` the only official MongoDB driver for Python. So what if I could instead use `mongodb` the *official* MongoDB client for Rust, which has native async APIs? And even maybe leverage Rust for static typing and validation and stuff.

I mean this is my personal example, but I'm sure you can come up with interesting and useful use-cases for using async Rust code from Python.
