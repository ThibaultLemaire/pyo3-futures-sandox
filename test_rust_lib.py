# python 3.8
import asyncio
import time
import uvloop

import awaitable_rust

async def foo(collection):
    await awaitable_rust.mongodb(collection)

async def main():
    collection = await awaitable_rust.MongoCollection.new()
    await asyncio.gather(*[foo(collection) for _ in range(5)])

async def main_list():
    collection = await awaitable_rust.MongoCollection.new()
    async def bar():
        return await collection.list_orwell_titles_alphabetically()
    print(await asyncio.gather(*[bar() for _ in range(5)]))

uvloop.install()
if __name__ == "__main__":
    asyncio.run(main_list())
