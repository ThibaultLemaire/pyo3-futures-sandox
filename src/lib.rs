use {
    futures::{
        future::{BoxFuture, FutureExt},
        stream::StreamExt,
        task::{waker_ref, ArcWake},
    },
    mongodb::{
        bson::{doc, Bson},
        options::{ClientOptions, FindOptions},
        Client,
    },
    pyo3::{
        callback::IntoPyCallbackOutput, exceptions::PyRuntimeError, iter::IterNextOutput,
        prelude::*, types::IntoPyDict, wrap_pyfunction, PyAsyncProtocol, PyIterProtocol,
    },
    std::{
        future::Future,
        pin::Pin,
        sync::{Arc, Mutex},
        task::{Context, Poll, Waker},
        thread,
        time::Duration,
    },
};

// Straight from Asynchronous Programming in Rust, I have removed the comments
// and commented what I changed.
pub struct TimerFuture {
    shared_state: Arc<Mutex<SharedState>>,
}

struct SharedState {
    /// This bool is now an option with our result
    result: Option<u32>,

    waker: Option<Waker>,
}

impl Future for TimerFuture {
    type Output = u32; // The Future now returns something
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut shared_state = self.shared_state.lock().unwrap();
        match shared_state.result {
            Some(result) => Poll::Ready(result),
            None => {
                shared_state.waker = Some(cx.waker().clone());
                Poll::Pending
            }
        }
    }
}

impl TimerFuture {
    /// The timer is now hardcoded to 1 and we take the result to return instead
    pub fn new(result: u32) -> Self {
        let shared_state = Arc::new(Mutex::new(SharedState {
            result: None,
            waker: None,
        }));

        // And here is the gist of it. We're essentially still doing the same thing:
        // Spawn a thread, wait 1s, square the result, set the result, wake the task
        let thread_shared_state = shared_state.clone();
        thread::spawn(move || {
            thread::sleep(Duration::from_secs(1));
            let mut shared_state = thread_shared_state.lock().unwrap();
            shared_state.result = Some(result * result);
            if let Some(waker) = shared_state.waker.take() {
                // Note here that we are still locking the shared state when
                // calling the waker. This will be relevant later.
                waker.wake()
            }
        });

        TimerFuture { shared_state }
    }
}

#[pyclass]
struct MyFuture {
    // Now we're encapsulating a Rust future
    future: Option<BoxFuture<'static, PyResult<PyObject>>>,
    aio_loop: Option<PyObject>,
    callbacks: Vec<(PyObject, Option<PyObject>)>,
    #[pyo3(get, set)]
    _asyncio_future_blocking: bool,
    // And we keep a reference to the waker that we will be passing to the
    // rust future
    waker: Option<Arc<WakerClosure>>,
}

impl MyFuture {
    // The constructor is now internal and takes a Future that must be Send + 'static
    // in order to be `.boxed()` (Python couldn't possibly pass us a Future anyway)
    fn new(
        future: impl Future<Output = impl IntoPyCallbackOutput<PyObject>> + Send + 'static,
    ) -> Self {
        Self {
            future: Some(
                async move {
                    let result = future.await;
                    Python::with_gil(move |py| result.convert(py))
                }
                .boxed(),
            ),
            aio_loop: None,
            callbacks: vec![],
            _asyncio_future_blocking: true,
            waker: None,
        }
    }
}

#[pyproto]
impl PyAsyncProtocol for MyFuture {
    fn __await__(slf: Py<Self>) -> PyResult<Py<Self>> {
        let py_future = slf.clone();
        Python::with_gil(|py| -> PyResult<_> {
            let mut slf = slf.try_borrow_mut(py)?;
            let aio_loop: PyObject = PyModule::import(py, "asyncio")?
                .call0("get_running_loop")?
                .into();
            slf.aio_loop = Some(aio_loop.clone());
            slf.waker = Some(Arc::new(WakerClosure {
                aio_loop,
                py_future,
            }));
            Ok(())
        })?;

        Ok(slf)
    }
}

#[pyproto]
impl PyIterProtocol for MyFuture {
    // Now MyFuture has truly become a Future wrapper
    fn __next__(mut slf: PyRefMut<Self>) -> PyResult<IterNextOutput<PyRefMut<Self>, PyObject>> {
        let mut future = slf.future.take().expect("no future");
        let waker = slf.waker.take().expect("no waker");
        let result = slf.py().allow_threads(|| {
            // Thanks to `futures` we don't need to implement the Waker pseudo-trait
            // manually, so this is all boiler-plate to pass the Waker to the Future.
            let waker_ref = waker_ref(&waker);
            let context = &mut Context::from_waker(&*waker_ref);
            future.as_mut().poll(context)
        });
        slf.future = Some(future);
        slf.waker = Some(waker);
        match result {
            Poll::Pending => {
                // In case the future needs to be put on hold multiple times,
                // we need to set this back to true every time we're in a
                // Pending state. Asyncio always sets it to false after
                // reading it when we yield ourself.
                slf._asyncio_future_blocking = true;
                Ok(IterNextOutput::Yield(slf))
            }
            Poll::Ready(result) => Ok(IterNextOutput::Return(result?)),
        }
    }
}

#[pyclass]
#[derive(Clone)]
struct WakerClosure {
    aio_loop: PyObject,
    py_future: Py<MyFuture>,
}

// WakerClosure now implements `futures::task::ArcWake` so it can be passed to a
// Rust Future as a waker, like we did above.
impl ArcWake for WakerClosure {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        let closure = (**arc_self).clone();
        Python::with_gil(|py| {
            arc_self
                .aio_loop
                .call_method1(py, "call_soon_threadsafe", (closure,))
        })
        .expect("exception thrown by the event loop (probably closed)");
        // Again, as a reminder, we are only setting ourself up to be called by
        // the event loop from the main thread so this child thread is released
        // before the task is woken up. Remember how the TimerFuture still has
        // its shared state locked when calling `.wake()`? This is where it
        // would have become a problem if we tried to call the callbacks directly.
        // Of course, here, we could fix the issue in the TimerFuture's code, but
        // in the wild we won't have control over how the waker is called.
        // (That state locking issue isn't something I made up or introduced on
        // purpose, it's genuinely the code from the async book.)
    }
}

#[pymethods]
impl WakerClosure {
    // I didn't change anything here, but as an exercise, try and move the code
    // from this function to `wake_by_ref` above to skip the double callback,
    // and see what happens.
    #[call]
    pub fn __call__(slf: PyRef<Self>) -> PyResult<()> {
        let py = slf.py();
        let mut py_future = slf.py_future.try_borrow_mut(py)?;
        if py_future.callbacks.is_empty() {
            panic!("nothing to call back")
        }
        let callbacks = std::mem::take(&mut py_future.callbacks);
        for (callback, context) in callbacks {
            slf.aio_loop.call_method(
                py,
                "call_soon",
                (callback, &py_future),
                Some(vec![("context", context)].into_py_dict(py)),
            )?;
        }
        Ok(())
    }
}

#[pymethods]
impl MyFuture {
    fn get_loop(&self) -> Option<&PyObject> {
        self.aio_loop.as_ref()
    }

    fn add_done_callback(&mut self, callback: PyObject, context: Option<PyObject>) {
        self.callbacks.push((callback, context));
    }

    fn result(&self) -> Option<PyObject> {
        None
    }
}

trait PyAwaitable:
    IntoPyCallbackOutput<*mut pyo3::ffi::PyObject> + PyAsyncProtocol<'static> + PyIterProtocol<'static>
{
}

impl<T> PyAwaitable for T where
    T: IntoPyCallbackOutput<*mut pyo3::ffi::PyObject>
        + PyAsyncProtocol<'static>
        + PyIterProtocol<'static>
{
}

trait IntoPyAwaitable<'p> {
    type Result: PyAwaitable;
    fn into_awaitable(self) -> Self::Result;
}

impl<T: Future<Output = impl IntoPyCallbackOutput<PyObject>> + Send + 'static> IntoPyAwaitable<'_>
    for T
{
    type Result = MyFuture;
    fn into_awaitable(self) -> Self::Result {
        MyFuture::new(self)
    }
}

// All that's left to do now is to wrap the TimerFuture in MyFuture, and everything
// works just as before.
// (You'll have to update the Python shim to use this function.)
#[pyfunction]
fn my_timer_future(result: u32) -> impl PyAwaitable {
    async move { TimerFuture::new(result).await }.into_awaitable()
}

#[pyfunction]
fn my_error_returning_future() -> impl PyAwaitable {
    async { PyRuntimeError::new_err("This is fine") }.into_awaitable()
}

#[pyfunction]
fn my_raising_future() -> impl PyAwaitable {
    async { Err::<(), _>(PyRuntimeError::new_err("This is not fine")) }.into_awaitable()
}

#[pyclass]
#[derive(Clone)]
struct MongoCollection {
    collection: mongodb::Collection,
}

impl MongoCollection {
    async fn _list_orwell_titles_alphabetically(
        &self,
    ) -> Result<Vec<Option<String>>, mongodb::error::Error> {
        let filter = doc! { "author": "George Orwell" };
        let find_options = FindOptions::builder().sort(doc! { "title": 1 }).build();
        let mut cursor = self.collection.find(filter, find_options).await?;
        let mut ret = vec![];

        while let Some(document) = cursor.next().await {
            ret.push(
                document?
                    .get("title")
                    .and_then(Bson::as_str)
                    .map(|s| s.to_owned()),
            );
        }
        Ok(ret)
    }
}

#[pymethods]
impl MongoCollection {
    #[staticmethod]
    fn new() -> impl PyAwaitable {
        async fn inner() -> Result<MongoCollection, mongodb::error::Error> {
            // Parse a connection string into an options struct.
            let mut client_options = ClientOptions::parse("mongodb://localhost:27017").await?;

            // Manually set an option.
            client_options.app_name = Some("My App".to_string());

            // Get a handle to the deployment.
            let client = Client::with_options(client_options)?;

            // Get a handle to a database.
            let db = client.database("mydb");

            // Get a handle to a collection in the database.
            Ok(MongoCollection {
                collection: db.collection("books"),
            })
        }
        async {
            match inner().await {
                Ok(collection) => Ok(Python::with_gil(move |py| Py::new(py, collection))?),
                Err(err) => Err(PyRuntimeError::new_err(format!("{:?}", err))),
            }
        }
        .into_awaitable()
    }

    fn list_orwell_titles_alphabetically(slf: PyRef<Self>) -> impl PyAwaitable {
        let slf = slf.clone();
        async move {
            match slf._list_orwell_titles_alphabetically().await {
                Ok(result) => Ok(result),
                // Dirty error handling for now, in a real library,
                // MongoDB errors would be mapped more nicely
                Err(err) => Err(PyRuntimeError::new_err(format!("{:?}", err))),
            }
        }
        .into_awaitable()
    }

    // In theory, the above method could be sugarised to:
    //
    // async fn list_orwell_titles_alphabetically(&self) -> PyResult<Vec<Option<String>>> {
    //     < body of `_list_orwell_titles_alphabetically` >
    // }
}

async fn list_db_names(collection: mongodb::Collection) -> Result<(), mongodb::error::Error> {
    let docs = vec![
        doc! { "title": "1984", "author": "George Orwell" },
        doc! { "title": "Animal Farm", "author": "George Orwell" },
        doc! { "title": "The Great Gatsby", "author": "F. Scott Fitzgerald" },
    ];

    // Insert some documents into the "mydb.books" collection.
    collection.insert_many(docs, None).await?;

    // Query the documents in the collection with a filter and an option.
    let filter = doc! { "author": "George Orwell" };
    let find_options = FindOptions::builder().sort(doc! { "title": 1 }).build();
    let mut cursor = collection.find(filter, find_options).await?;

    // Iterate over the results of the cursor.
    while let Some(result) = cursor.next().await {
        match result {
            Ok(document) => {
                if let Some(title) = document.get("title").and_then(Bson::as_str) {
                    println!("title: {}", title);
                } else {
                    println!("no title found");
                }
            }
            Err(e) => return Err(e.into()),
        }
    }

    let doc = doc! {
     "email": "email@example.com",
     "name": {"given": "Jesse", "family": "Xiao"},
     "age": 31,
     "addresses": [{"label": "home",
                  "street": "101 Elm Street",
                  "city": "Springfield",
                  "state": "CA",
                  "zip": "90000",
                  "country": "US"},
                 {"label": "mom",
                  "street": "555 Main Street",
                  "city": "Jonestown",
                  "province": "Ontario",
                  "country": "CA"}]

    };

    let docs = vec![
        doc.clone(),
        doc.clone(),
        doc.clone(),
        doc.clone(),
        doc.clone(),
    ];

    loop {
        collection.insert_many(docs.clone(), None).await?;
        println!(
            "estimated_document_count: {}",
            collection.estimated_document_count(None).await?,
        );
    }
}

#[pyfunction]
fn mongodb(collection: &MongoCollection) -> impl PyAwaitable {
    let collection = collection.collection.clone();
    async move {
        match list_db_names(collection).await {
            Ok(()) => Ok(()),
            Err(err) => Err(PyRuntimeError::new_err(format!("{:?}", err))),
        }
    }
    .into_awaitable()
}

#[pymodule]
fn awaitable_rust(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<MyFuture>()?;
    m.add_class::<MongoCollection>()?;
    m.add_function(wrap_pyfunction!(my_timer_future, m)?)?;
    m.add_function(wrap_pyfunction!(my_error_returning_future, m)?)?;
    m.add_function(wrap_pyfunction!(my_raising_future, m)?)?;
    m.add_function(wrap_pyfunction!(mongodb, m)?)?;
    Ok(())
}
