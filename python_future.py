# python 3.8
import asyncio
from asyncio.exceptions import InvalidStateError
import threading
import time


class MyFuture:
    def __init__(self, result):
        self._result = result
        self._loop = None
        self._callbacks = []
        self._asyncio_future_blocking = True

    def __await__(self):
        self._loop = asyncio.get_running_loop()

        def sleep_and_return(result):
            time.sleep(1)
            self._loop.call_soon_threadsafe(self._set_result, result * result)

        threading.Thread(target=sleep_and_return, args=(self._result,)).start()
        self._result = None
        return self

    def __next__(self):
        if self._result is None:
            return self
        raise StopIteration(self._result)

    def get_loop(self):
        return self._loop

    def add_done_callback(self, callback, *, context=None):
        self._callbacks.append((callback, context))
    
    def _set_result(self, result):
        self._result = result
        for cb, ctx in self._callbacks:
            self._loop.call_soon(cb, self, context=ctx)
        self._callbacks[:] = []

    def result(self):
        return None

async def wrapper(i):
    return await MyFuture(i)

async def main():
    results = await asyncio.gather(*[wrapper(i) for i in range(1000)])
    print(results)

asyncio.run(main())